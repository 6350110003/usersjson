// To parse this JSON data, do
//
//     final datamodel = datamodelFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

List<Datamodel> datamodelFromJson(String str) => List<Datamodel>.from(json.decode(str).map((x) => Datamodel.fromJson(x)));

String datamodelToJson(List<Datamodel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Datamodel {
  Datamodel({
    required this.username,
    required this.email,
    required this.urlAvatar,
  });

  final String username;
  final String email;
  final String urlAvatar;

  factory Datamodel.fromJson(Map<String, dynamic> json) => Datamodel(
    username: json["username"],
    email: json["email"],
    urlAvatar: json["urlAvatar"],
  );

  Map<String, dynamic> toJson() => {
    "username": username,
    "email": email,
    "urlAvatar": urlAvatar,
  };
}
